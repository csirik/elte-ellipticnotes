\contentsline {chapter}{Introduction}{3}
\contentsline {chapter}{\numberline {1}Numerical Methods for Elliptic Partial Differential Equations}{4}
\contentsline {section}{\numberline {1.1}Background of Linear Elliptic Problems}{4}
\contentsline {subsection}{\numberline {1.1.1}Physical background of elliptic problems}{4}
\contentsline {subsection}{\numberline {1.1.2}Sobolev spaces, Lax--Milgram theory, weak-, and regular solutions}{7}
\contentsline {subsubsection}{(a) Sobolev spaces}{7}
\contentsline {subsubsection}{(b) Lax--Milgram theory, weak-, and regular solutions}{9}
\contentsline {section}{\numberline {1.2}Finite differences}{13}
\contentsline {subsection}{\numberline {1.2.1}Preliminaries}{13}
\contentsline {subsubsection}{(a) Basic discretization scheme}{13}
\contentsline {subsubsection}{(b) Basic properties of $M$-matrices}{15}
\contentsline {subsection}{\numberline {1.2.2}Finite differences for Poisson's equation on a rectangle}{16}
\contentsline {paragraph}{Problem formulation.}{16}
\contentsline {paragraph}{Approximating the Laplacian by finite differences}{17}
\contentsline {paragraph}{Approximating nodal values.}{19}
\contentsline {paragraph}{The equation $A_h \overline u^h = f^h$.}{22}
\contentsline {subsection}{\numberline {1.2.3}Stability and convergence}{23}
\contentsline {paragraph}{Stability of the approximate solution.}{23}
\contentsline {paragraph}{Convergence of FDM in maximum norm.}{24}
\contentsline {subsection}{\numberline {1.2.4}Stability and error estimates in $L^2$ and $H^1_0$}{25}
\contentsline {paragraph}{Basic notions and estimates.}{25}
\contentsline {paragraph}{Eigenproblem for the discrete Laplacian on a rectangle.}{27}
\contentsline {subsection}{\numberline {1.2.5}Finite differences for more general problems}{29}
\contentsline {paragraph}{General domain.}{29}
\contentsline {paragraph}{Inhomogeneous boundary conditions.}{31}
\contentsline {paragraph}{Mixed boundary conditions.}{32}
\contentsline {paragraph}{Variable-coeffient PDEs.}{32}
\contentsline {section}{\numberline {1.3}The finite element method}{33}
\contentsline {subsection}{\numberline {1.3.1}Galerkin method}{34}
\contentsline {subsubsection}{(a) Galerkin method for bilinear forms}{34}
\contentsline {subsubsection}{(b) C{\'e}a's lemma, quasi-optimality and convergence}{38}
\contentsline {subsection}{\numberline {1.3.2}Construction of the FEM. Types of finite elemens}{40}
\contentsline {subsubsection}{(a) Construction of the finite element method for general symmetric elliptic problems}{40}
\contentsline {subsubsection}{(b) Finite elements and their types}{42}
\contentsline {subsection}{\numberline {1.3.3}Stability of the finite element method}{56}
\contentsline {subsection}{\numberline {1.3.4}Convergence of the finite element method}{58}
\contentsline {subsubsection}{(a) Introduction: convergence and interpolation}{58}
\contentsline {subsubsection}{(b) First-order convergence estimate for Courant elements}{59}
\contentsline {subsubsection}{(c) Convergence without regularity}{67}
\contentsline {subsection}{\numberline {1.3.5}Higher-order interpolation and convergence, Bramble--Hilbert lemma}{68}
\contentsline {subsubsection}{(a) Interpolation estimates in the $H^1$ norm}{68}
\contentsline {subsubsection}{(b) Higher-order convergence of the finite element method}{70}
\contentsline {subsubsection}{(c) Interpolation estimates in higher-order $H^\ell $ norms}{71}
\contentsline {subsection}{\numberline {1.3.6}More convergence estimates, Nitsche's trick, numerical integration}{72}
\contentsline {subsubsection}{(a) Comparison of the convergence of FDM and FEM}{72}
\contentsline {subsubsection}{(b) Nitsche's trick, convergence estimates in $L^2$}{73}
\contentsline {subsubsection}{(c) The effect of numerical integration}{74}
\contentsline {subsubsection}{(d) Refining the mesh, adaptive finite element method}{77}
\contentsline {subsection}{\numberline {1.3.7}Nonsymmetric and fourth-order equations}{79}
\contentsline {subsubsection}{\bf (a) Nonsymmetric equations}{79}
\contentsline {subsubsection}{\bf (b) Fourth-order equations}{87}
\contentsline {section}{\numberline {1.4}Iterative solution of the discretized problems}{90}
\contentsline {subsection}{\numberline {1.4.1}Direct solution, Fourier method}{90}
\contentsline {subsection}{\numberline {1.4.2}Simple iteration}{90}
\contentsline {subsection}{\numberline {1.4.3}The conjugate gradient method}{92}
\contentsline {subsection}{\numberline {1.4.4}Preconditioning}{93}
\contentsline {section}{\numberline {1.5}The multigrid method}{97}
\contentsline {subsection}{\numberline {1.5.1}Basic principles and construction of the two-grid method}{97}
\contentsline {subsection}{\numberline {1.5.2}The convergence of the two-grid method}{102}
\contentsline {subsection}{\numberline {1.5.3}Multigrid algorithms}{107}
\contentsline {section}{\numberline {1.6}Numerical solution of saddle-point problems}{111}
\contentsline {subsection}{\numberline {1.6.1}Existence, inf-sup condition}{111}
\contentsline {subsection}{\numberline {1.6.2}The Uzawa algorithm}{116}
\contentsline {paragraph}{Absztrakt Uzawa-iter\IeC {\'a}ci\IeC {\'o}.}{116}
\contentsline {paragraph}{Uzawa-iter\IeC {\'a}ci\IeC {\'o} a Stokes-feladatra.}{117}
\contentsline {subsection}{\numberline {1.6.3}Finite element solution of the Stokes problem}{118}
\contentsline {section}{\numberline {1.7}Numerical solution of nonlinear elliptic problems}{121}
\contentsline {subsection}{\numberline {1.7.1}A few simple model problems}{121}
\contentsline {paragraph}{Skal\IeC {\'a}r-nemlinearit\IeC {\'a}s.}{121}
\contentsline {paragraph}{F\IeC {\H o}r\IeC {\'e}sz\IeC {\'e}ben nemline\IeC {\'a}ris divergencia-alak\IeC {\'u} feladatok.}{122}
\contentsline {paragraph}{Szemiline{\'a}ris konvekci\IeC {\'o}-reakci\IeC {\'o}-diff\IeC {\'u}zi\IeC {\'o}s feladatok.}{123}
\contentsline {subsection}{\numberline {1.7.2}Monotone operators, existence}{124}
\contentsline {subsection}{\numberline {1.7.3}Finite element discretization}{125}
\contentsline {paragraph}{Galjorkin-m{\'o}dszer nemline{\'a}ris operatoregyenletekre.}{125}
\contentsline {paragraph}{V{\'e}geselemes megold{\'a}s nemlinear\ elliptikus feladatra.}{126}
\contentsline {subsection}{\numberline {1.7.4}Newton-type iterations}{127}
\contentsline {section}{\numberline {1.8}Exercises}{132}
